<?php

namespace Drupal\linkback_semantic\Plugin\Block;

use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * Provides a 'SemanticLinkbacks' block.
 *
 * @Block(
 *  id = "semantic_linkbacks",
 *  admin_label = @Translation("Semantic linkbacks"),
 *  context = {
 *    "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *  }
 * )
 */
class SemanticLinkbacks extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Service to get list of available fields for entity.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $fieldManager;

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityFieldManager $field_manager
   *   The service to get list of available fields for entity.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The service to work with entity Types.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        EntityFieldManager $field_manager,
        EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->fieldManager = $field_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'show_linkbacks_by_semantic_type' => [],
    ] + parent::defaultConfiguration();

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['show_linkbacks_by_semantic_type'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show list of linkbacks by semantic type'),
      '#default_value' => $this->configuration['show_linkbacks_by_semantic_type'],
      '#description' => $this->t('Uncheck if you want to use a custom view of the list of linkbacks.'),
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'view published linkback entities');
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['show_linkbacks_by_semantic_type'] = $form_state->getValue('show_linkbacks_by_semantic_type');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->getContextValue('node');
    $nid = $node->id();
    if (!isset($this->fieldManager->getFieldMap()['linkback']['semantic_type'])) {
      $build = [];
      $build['semantic_linkbacks']['#markup'] = $this->t('field "semantic type" not available');
      return $build;
    }
    try {
      $linkback_count = $this->entityTypeManager->getStorage('linkback')->getAggregateQuery()
        ->condition('ref_content', $nid)
        ->condition('status', 1)
        ->condition('semantic_type', NULL, 'IS NOT NULL')
        ->aggregate('id', 'COUNT')
        ->groupBy('semantic_type')
        ->execute();
    }
    catch (QueryException $e) {
      $build = [];
      $build['semantic_linkbacks']['#markup'] = "Configuration error: " . $e->getMessage();
      return $build;
    }
    $build = [];
    $items = [];
    if (!empty($linkback_count)) {
      foreach ($linkback_count as $key => $semtype) {
        $items[$key]['semantic_type']['name_link'] = [
          '#title' => $semtype['semantic_type_value'],
          '#type' => 'link',
          '#url' => Url::fromRoute('<current>', [], ['fragment' => 'semantic-linkbacks-' . $semtype['semantic_type_value']]),
        ];
        $items[$key]['semantic_type']['anchor'] = new Attribute(['href' => '#semantic-linkbacks-' . $semtype['semantic_type_value']]);
        $items[$key]['semantic_type']['name'] = $semtype['semantic_type_value'];
        $items[$key]['semantic_type']['attributes'] = new Attribute(['class' => $semtype['semantic_type_value']]);
        $items[$key]['semantic_type']['icon_class'] = 'icon-' . $semtype['semantic_type_value'];
        $items[$key]['linkbacks_counter'] = $semtype['id_count'];
      }
    }
    $build['semantic_linkbacks_counter'] = [
      '#theme' => 'semantic_linkbacks_counter',
      '#items' => $items,
      '#information' => $this->t("This site uses semantic webmentions to interact between sites and build federated conversations."),
    ];
    if ($this->configuration['show_linkbacks_by_semantic_type']) {
      foreach ($linkback_count as $key => $semtype) {
        $semantic = $semtype['semantic_type_value'];
        $linkbacks = $this->entityTypeManager->getStorage('linkback')->getQuery()
          ->condition('ref_content', $nid)
          ->condition('status', 1)
          ->condition('semantic_type', $semantic, "=")
          ->sort('created')
          ->addMetadata('semantic_field', 'semantic_type')
          ->addTag('by_semantic')
          ->execute();
        $build['semantic_linkbacks_counter']['#linkbacks'][$semantic] = [
          '#type' => 'container',
          '#attributes' => [
            'id' => "semantic-linkbacks-wrapper-" . $semantic,
          ],
        ];
        $build['semantic_linkbacks_counter']['#linkbacks'][$semantic]['title'] = [
          '#type' => 'html_tag',
          '#value' => $semantic,
          '#tag' => 'div',
        ];
        $build['semantic_linkbacks_counter']['#linkbacks'][$semantic]['title_attributes'] = new Attribute(['id' => 'semantic-linkbacks-' . $semantic]);
        $linkback_storage = $this->entityTypeManager->getStorage('linkback');
        $linkback_objects = $linkback_storage->loadMultiple($linkbacks);
        $view_builder = $this->entityTypeManager->getViewBuilder('linkback');
        $linkbacks_render = $view_builder->viewMultiple($linkback_objects, 'default');
        $build['semantic_linkbacks_counter']['#linkbacks'][$semantic]['linkbacks'] = $linkbacks_render;
      }
    }
    return $build;
  }

}
