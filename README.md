#Linkback semantic
Creates a new field called semantic type in linkback entity. If the linkback has fetched some metainfo and has some microformats semantics such as:
* 'in-reply-to'
* 'like-of'
* 'repost-of'
* 'bookmark-of'
* 'tag-of'
Will include it in the new field semantic type. This way you can create views grouping by semantic type.

It also provides a block with semantic linkbacks statistics for each content. And optionally a list of linkbacks grouped by semantic type if you don't want to create a new view. This block is themeable, override included template in your theme to render as you would.
